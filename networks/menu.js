import axios from 'axios';
const TOKEN = 'MNEATc87fN8CnyzJOYrBNfyQCsmDvwU7PitKog4Z0QPmz89diIEvSEBu1rOeoIl3';

export const getMenu = async () => {
    return axios.get(`/api/menu`).then(response => {
        return response;
    });
}


export const updateMenu = formData => {
    return axios.post(`https://avemenu.app/api/cafe/off/menu/edit`, formData, {
        headers: {
            'Access-Token': TOKEN,
            'Content-Type': 'multipart/form-data',
        },
    });
};

export const createMenu = formData => {
    return axios.post(`https://avemenu.app/api/cafe/off/menu/create`, formData, {
        headers: {
            'Access-Token': TOKEN,
            'Content-Type': 'multipart/form-data',
        },
    });
};

export const removeFood = (cafe_id, food_id, imgName) => {
    return axios({
        method: 'DELETE',
        url: `https://avemenu.app/api/cafe/off/menu/remove`,
        headers: {
            'Access-Token': TOKEN,
            'Content-Type': 'application/json',
        },
        data: {
            cafe_id,
            food_id,
            imgName,
        },
    });
};
