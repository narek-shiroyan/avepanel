import axios from 'axios';

const TOKEN = 'MNEATc87fN8CnyzJOYrBNfyQCsmDvwU7PitKog4Z0QPmz89diIEvSEBu1rOeoIl3';


export const getCafes = async () => {
  return axios.get(`/api/cafe`).then(response => {
    return response;
  });
}

export const createCafe = async (items) => {
  return axios({
    method: 'POST',
    url: `/api/cafe`,
    data: {
      items
    },
  });

}

// export const getCafeById = async (id) => {
//   return axios({
//         method: 'GET',
//         url: `https://avemenu.app/api/cafe/${id}`,
//         headers: {
//             'Access-Token': TOKEN,
//         },
//     });
// }

export const getCafeById = async (id) => {
  return axios.get(`/api/cafe/${id}`).then(response => {
    return response;
  });
}





export const addCafeImage = async formData => {
  return axios.post(`https://avemenu.app/api/cafe/images/`, formData, {
    // headers: {
    //   'Access-Token': TOKEN,
    //   'Content-Type': 'multipart/form-data',
    // },
  });
}

export const deleteCafeImage = async (imgName, cafe_id) => {

  return axios({
    method: 'DELETE',
    url: `https://avemenu.app/api/cafe/images/`,
    data: {
      imgName, cafe_id
    },
  });

}

export const updateCafe = async (id, info) => {
  return axios({
    method: 'PUT',
    url: `/api/cafe/${id}`,
    data: {
      id, info
    },
  });

}

export const addTable = async (id, table, tableName) => {
  return axios({
    method: 'POST',
    url: `/api/cafe/table`,
    data: {
      id, table, tableName
    },
  });
}

export const deleteTable = async (cafe_id, table_id) => {
  return axios({
    method: 'DELETE',
    url: `/api/cafe/table`,
    data: {
      cafe_id, table_id
    },
  });
}


export const deleteCafe = async (cafe_id) => {
  return axios({
    method: 'DELETE',
    url: `https://avemenu.app/api/cafe`,
    headers: {
      'Access-Token': TOKEN,
    },
    data: {
      cafe_id,
    },
  });
}