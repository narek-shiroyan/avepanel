import axios from 'axios';


export const getAdmins = async () => {
    return axios.get(`/api/admin`).then(response => {
        return response;
    });
}

export const createAdmin = async (login, password, email) => {
    return axios({
        method: 'POST',
        url: `/api/create`,
        data: {
            login,
            password,
            email
        },
    });
}

export const createStaff = async (login, password, cafe) => {
    return axios({
        method: 'POST',
        url: `/api/staff`,
        data: {
            login,
            password,
            cafe
        },
    });
}

export const _createTableBase = async (name, table, cafeId) => {
    return axios({
        method: 'POST',
        url: `/api/tables`,
        data: {
            name,
            table,
            cafeId,
        },
    });
}


export const _getTableCodes = async (cafeId) => {
    return axios({
        method: 'POST',
        url: `/api/getTables`,
        data: {
            cafeId,
        },
    });
}
