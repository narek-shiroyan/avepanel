import Resizer from "react-image-file-resizer";


export const resizeFile = (file, maxWidth) =>
  new Promise((resolve) => {
    Resizer.imageFileResizer(
      file,
      maxWidth,
      (uri) => {
        resolve(uri);
      },
      "base64"
    );
  });