import InputLabel from "@material-ui/core/InputLabel";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import Search from "@material-ui/icons/Search";
import { CircularProgress } from "@material-ui/core";
import Card from "components/Card/Card.js";
import CardAvatar from "components/Card/CardAvatar.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import CardHeader from "components/Card/CardHeader.js";
import Button from "components/CustomButtons/Button.js";
// core components
import CustomInput from "components/CustomInput/CustomInput.js";
import CafeSelect from 'components/CustomSelect/CafeSelect';
// dialog
import CreateMenuDialog from "components/Dialog/CreateMenu";
import GridContainer from "components/Grid/GridContainer.js";
// core components
import GridItem from "components/Grid/GridItem.js";
import MenuList from "components/Tasks/MenuList";
// layout for this page
import Admin from "layouts/Admin.js";
import { getCafes, getCafeById } from 'networks/cafe';


import React, { useEffect, useState } from "react";
import EditMenuDialog from "components/Dialog/EditMenu";
import { removeFood } from "networks/menu";


const styles = {
    cardCategoryWhite: {
        color: "rgba(255,255,255,.62)",
        margin: "0",
        fontSize: "14px",
        marginTop: "0",
        marginBottom: "0",
    },
    cardTitleWhite: {
        color: "#FFFFFF",
        marginTop: "0px",
        minHeight: "auto",
        fontWeight: "300",
        fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
        marginBottom: "3px",
        textDecoration: "none",
    },
};

function Menu() {
    const useStyles = makeStyles(styles);
    const classes = useStyles();
    const [cafes, setCafes] = useState([]);                 // Загружаем сюда кафешки, для выбора
    const [selectCafe, setSelectCafe] = useState(null);     // Выбранное кафе
    const [menuItems, setMenuItems] = useState([]);         // Меню для отображения в списке
    const [showCreate, setShowCreate] = useState(false);    // Для всплывашки создания меню
    const [showEdit, setShowEdit] = useState(false);        // Для всплывашки редактирования меню
    const [selectMenu, setSelectMenu] = useState(null);     // Для выбора редактируемого меню
    const [loading, setLoading] = useState(true);           // Загрузка


    useEffect(() => {
        loadCafes();
    }, []);



    if (loading) {
        return <CircularProgress />
    }


    return (
        <div>
            <GridContainer>
                <GridItem xs={12} sm={12} md={8}>
                    <Card>
                        <CardHeader color="dark">
                            <h4 className={classes.cardTitleWhite}>Заведение</h4>

                        </CardHeader>
                        <CardBody>

                            <GridContainer>
                                <GridItem xs={12} sm={12} md={12}>
                                    <InputLabel>Выберите заведение</InputLabel>
                                    <CafeSelect
                                        labelText="Выберите заведение"
                                        id="cafes"
                                        owners={cafes}
                                        selected={selectCafe}
                                        handleChange={(event) => {
                                            setSelectCafe(event.target.value);
                                            setMenuItems(event.target.value.menu);
                                        }}
                                        formControlProps={{
                                            fullWidth: true,
                                        }}
                                    />
                                </GridItem>
                            </GridContainer>
                        </CardBody>
                        <CardFooter>

                        </CardFooter>
                    </Card>
                    {
                        selectCafe ? (
                            <Card>
                                <CardHeader color="dark">
                                    <h4 className={classes.cardTitleWhite}>Меню</h4>

                                </CardHeader>
                                <CardBody>

                                    <GridContainer>
                                        <GridItem xs={12} sm={12} md={12}>
                                            {renderMenu()}
                                        </GridItem>
                                    </GridContainer>
                                </CardBody>
                                <CardFooter>

                                </CardFooter>
                            </Card>
                        ) : null
                    }


                </GridItem>


                {
                    selectCafe ? (
                        <GridItem xs={12} sm={12} md={4}>
                            <Card profile>
                                <CardAvatar profile>
                                    <a href="#pablo" onClick={(e) => e.preventDefault()}>
                                        {
                                            selectCafe.images.length ? (
                                                <>
                                                    <img
                                                        src={`https://avemenu.app/api/images/${selectCafe.images[0]}/500/500`}
                                                        alt={`${selectCafe.name}`} />
                                                </>
                                            ) : (
                                                <p>Нет картинки</p>
                                            )
                                        }
                                    </a>
                                </CardAvatar>
                                <CardBody profile>
                                    <h4 className={classes.cardTitle}>{selectCafe.name}</h4>
                                    <p className={classes.description}>
                                        {selectCafe.description}
                                    </p>
                                    {/* <Button color="primary" round>
                                        Follow
                                    </Button> */}
                                </CardBody>
                            </Card>
                        </GridItem>
                    ) : null
                }
            </GridContainer>
            <CreateMenuDialog
                show={showCreate}
                cafe_id={selectCafe ? selectCafe._id : null}
                handleClose={() => setShowCreate(false)}
                handleRefresh={refreshCafe}
            />

            <EditMenuDialog
                cafe_id={selectCafe ? selectCafe._id : null}
                handleClose={() => setShowEdit(false)}
                handleRefresh={refreshCafe}
                menu={selectMenu}
                show={showEdit}
            />
        </div>
    );



    function renderMenu() {
        if (!selectCafe.menu.length) {
            return (
                <div>
                    <p>Нет меню</p>
                    <Button color="white" round onClick={showCreateDialog}>
                        Создать
                    </Button>
                </div>
            )
        }

        // Если после поиска попали в пустоту
        if (selectCafe.menu.length && !menuItems.length) {
            return (
                <>
                    <div>
                        <CustomInput
                            formControlProps={{
                                className: classes.margin + " " + classes.search,
                            }}

                            onChange={(event) => handleSearch(event.target.value)}
                            inputProps={{
                                placeholder: "Поиск блюд",
                                inputProps: {
                                    "aria-label": "Поиск",
                                },
                            }}
                        />
                        <Button color="white" aria-label="edit" justIcon round>
                            <Search />
                        </Button>

                        <Button color="white" round onClick={showCreateDialog}>
                            Создать
                        </Button>

                        <p>Ничего не найдено</p>
                    </div>

                </>
            )
        }

        return (
            <>
                <div>
                    <CustomInput
                        formControlProps={{
                            className: classes.margin + " " + classes.search,
                        }}

                        onChange={(event) => handleSearch(event.target.value)}
                        inputProps={{
                            placeholder: "Поиск блюд",
                            inputProps: {
                                "aria-label": "Поиск",
                            },
                        }}
                    />
                    <Button color="white" aria-label="edit" justIcon round>
                        <Search />
                    </Button>
                    <Button color="white" round onClick={showCreateDialog}>
                        Создать
                    </Button>
                </div>
                <MenuList list={menuItems}
                    click={m => {
                        setSelectMenu(m);
                        setShowEdit(true);
                    }}
                    cafe_id={selectCafe._id}
                    deleteFood={deleteMenu}
                    />
            </>
        )
    }


    async function loadCafes() {
        try {
            const { data } = await getCafes();
            setCafes(data.items);
            setLoading(false);
        } catch (e) {
            console.log(e);
            setLoading(false);
        }
    }

    async function deleteMenu(cafe_id, food_id, imgName) {
        const q = confirm('Вы действительно хотите удалить блюдо?');
        if (q) {
            const { data } = await removeFood(cafe_id, food_id, imgName);
            if (data.status === 'ERROR') {
                alert('Ошибка');
            }
            if (data.status === 'OK') {
                await refreshCafe();
            }
        }
    }

    function showCreateDialog() {
        setShowCreate(true);
    }

    async function refreshCafe() {
        setLoading(true);
        try {
            const { data } = await getCafeById(selectCafe._id);

            if (data.status === 'ERROR') {
                alert(data.message);
            } else {
                setSelectCafe(data.items);
                setMenuItems(data.items.menu);
            }
            setLoading(false);
            setShowCreate(false);
            setShowEdit(false);

        } catch (e) {
            console.log(e);
            setLoading(false);
            setShowCreate(false);
            setShowEdit(false);
        }
    }


    function handleSearch(t) {
        // Взять текст из инпута t
        // Фильтром пройтись и каждое поле name проверить на вхождение t
        const word = t.toLowerCase();
        const newArr = selectCafe.menu.filter((v) => v.name.toLowerCase().includes(word));
        setMenuItems(newArr);
    }
}

Menu.layout = Admin;

export default Menu;
