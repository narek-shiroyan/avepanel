// @material-ui/core components
import { CircularProgress } from "@material-ui/core";
import InputLabel from "@material-ui/core/InputLabel";
import { makeStyles } from "@material-ui/core/styles";
import { BugReport } from '@material-ui/icons/BugReport';
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import CardHeader from "components/Card/CardHeader.js";
import Button from "components/CustomButtons/Button.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import CustomSelect from "components/CustomSelect/CustomSelect";
import CustomTabs from 'components/CustomTabs/CustomTabs';
import GridContainer from "components/Grid/GridContainer.js";
// core components
import GridItem from "components/Grid/GridItem.js";
import TasksWithout from 'components/Tasks/TasksWithout';
// layout for this page
import Admin from "layouts/Admin.js";
import { getAdmins } from 'networks/admin';
import { createCafe, getCafes, deleteCafe } from 'networks/cafe';
import React, { useEffect, useState } from "react";

const styles = {
  cardCategoryWhite: {
    color: "rgba(255,255,255,.62)",
    margin: "0",
    fontSize: "14px",
    marginTop: "0",
    marginBottom: "0",
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
  },
};

function Cafes() {
  const useStyles = makeStyles(styles);
  const classes = useStyles();

  const [cafes, setCafes] = useState([]);
  const [admins, setAdmins] = useState([]);
  const [loading, setLoading] = useState(true);

  // Переменная для отправки на сервер
  const [info, setInfo] = useState({
    //pickedAdmin: null,
    name: null,
    description: null,
    email: null,
    address: null,
    work_time: null,
    owner: null,
    tables: [],
    website: null,
    lat: null,
    lon: null,
    phone: null,
  })
  useEffect(() => {
    loadData();
  }, []);

  if (loading) {
    return <CircularProgress />
  }
  return (
    <div>
      <GridContainer>
        <GridItem xs={12} sm={12} md={6}>
          <Card>
            <CardHeader color="dark">
              <h4 className={classes.cardTitleWhite}>Добавить заведение</h4>
              <p className={classes.cardCategoryWhite}>Введите данные</p>
            </CardHeader>
            <CardBody>
              <GridContainer>
                <GridItem xs={12} sm={12} md={12}>
                  <CustomInput
                    labelText="Название"
                    id="title"
                    formControlProps={{
                      fullWidth: true,
                    }}
                    onChange={(event) => setInfo({
                      ...info,
                      name: event.target.value,
                    })}
                    inputProps={{
                      // disabled: true,
                    }}
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={12}>
                  <CustomInput
                    labelText="Описание"
                    id="description"
                    formControlProps={{
                      fullWidth: true,
                    }}
                    onChange={(event) => setInfo({
                      ...info,
                      description: event.target.value,
                    })}
                    inputProps={{
                      multiline: true,
                      rows: 5,
                    }}
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={6}>
                  <CustomInput
                    labelText="Email"
                    id="email"
                    onChange={(event) => setInfo({
                      ...info,
                      email: event.target.value,
                    })}
                    formControlProps={{
                      fullWidth: true,
                    }}
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={6}>
                  <CustomInput
                    labelText="Режим работы"
                    id="work"
                    onChange={(event) => setInfo({
                      ...info,
                      work_time: event.target.value,
                    })}
                    formControlProps={{
                      fullWidth: true,
                    }}
                  />
                </GridItem>
              </GridContainer>
              <GridContainer>
                <GridItem xs={12} sm={12} md={12}>
                  <CustomInput
                    labelText="Адрес"
                    id="address"
                    onChange={(event) => setInfo({
                      ...info,
                      address: event.target.value,
                    })}
                    formControlProps={{
                      fullWidth: true,
                    }}
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={6}>
                  <CustomInput
                    labelText="lat"
                    id="lat"
                    onChange={(event) => setInfo({
                      ...info,
                      lat: event.target.value,
                    })}
                    formControlProps={{
                      fullWidth: true,
                    }}
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={6}>
                  <CustomInput
                    labelText="lon"
                    id="lon"
                    onChange={(event) => setInfo({
                      ...info,
                      lon: event.target.value,
                    })}
                    formControlProps={{
                      fullWidth: true,
                    }}
                  />
                </GridItem>
              </GridContainer>
              <GridContainer>
                <GridItem xs={12} sm={12} md={4}>
                  <CustomInput
                    labelText="Сайт"
                    id="site"
                    onChange={(event) => setInfo({
                      ...info,
                      website: event.target.value,
                    })}

                    formControlProps={{
                      fullWidth: true,
                    }}
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={4}>
                  <CustomInput
                    labelText="Номер телефона"
                    id="phone"
                    onChange={(event) => setInfo({
                      ...info,
                      phone: event.target.value,
                    })}
                    formControlProps={{
                      fullWidth: true,
                    }}
                  />
                </GridItem>

              </GridContainer>
              <GridContainer>
                <GridItem xs={12} sm={12} md={12}>
                  <InputLabel>Выбрать владельца</InputLabel>
                  <CustomSelect
                    labelText="Владелец"
                    id="owner"
                    owners={admins}
                    selected={info.owner}
                    handleChange={(event) => setInfo({
                      ...info,
                      owner: event.target.value
                    })}
                    formControlProps={{
                      fullWidth: true,
                    }}
                  />
                </GridItem>

                {/* <GridItem xs={12} sm={12} md={12}>
                  <InputLabel>Выбрать картинки</InputLabel>
                  <ImagePicker
                    id="owner"
                    formControlProps={{
                      fullWidth: true,
                    }}
                  />
                </GridItem> */}
              </GridContainer>
            </CardBody>
            <CardFooter>
              <Button color="dark" onClick={createCafes}>Создать</Button>
            </CardFooter>
          </Card>
        </GridItem>
        <GridItem xs={12} sm={12} md={6}>
          <h1>Заведения</h1>
          <CustomTabs
            title="Заведения:"
            headerColor="dark"
            tabs={[
              {
                tabName: "",
                tabIcon: BugReport,
                tabContent: (
                  <TasksWithout
                    tasks={cafes}
                    handleDelete={handleDeleteCafe}
                  />
                ),
              },
            ]}
          />
        </GridItem>
      </GridContainer>
    </div>
  );

  async function loadData() {
    setLoading(true);

    try {
      const cafe = await getCafes();
      const admin = await getAdmins();
      setCafes(cafe.data.items);
      setAdmins(admin.data.items);
      setLoading(false);

    } catch (e) {
      console.log(e);
      setLoading(false);
    }
  }

  async function handleDeleteCafe(cafe_id) {
    const q = confirm('Вы действительно хотите удалить заведение?');
    if (q) {
      try {
        const { data } = await deleteCafe(cafe_id);
        console.log('DATA', data);
        if (data.status === 'OK') {
          await loadData();
        }
      } catch (e) {
        console.log(e);
      }

    }
  }

  async function createCafes() {
    setLoading(true);
    try {
      const { data } = await createCafe(info);
      //console.log('data', data);

      //setLoading(false);
      await loadData();
    } catch (e) {
      console.log(e);
      setLoading(false);
    }


  }
}

Cafes.layout = Admin;

export default Cafes;
