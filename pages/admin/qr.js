import React, { useEffect, useState } from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
// layout for this page
import Admin from "layouts/Admin.js";
// core components
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardAvatar from "components/Card/CardAvatar.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import { CircularProgress } from "@material-ui/core";
import CafeSelect from 'components/CustomSelect/CafeSelect';
import { getCafes, getCafeById } from 'networks/cafe';


import avatar from "assets/img/faces/marc.jpg";
import { _getTableCodes } from "../../networks/admin";
import QRTables from "components/Table/QRTables";

const styles = {
  cardCategoryWhite: {
    color: "rgba(255,255,255,.62)",
    margin: "0",
    fontSize: "14px",
    marginTop: "0",
    marginBottom: "0",
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
  },
};

function QRPage() {
  const useStyles = makeStyles(styles);
  const classes = useStyles();

  const [cafes, setCafes] = useState([]);                 // Загружаем сюда кафешки, для выбора
  const [selectCafe, setSelectCafe] = useState(null);     // Выбранное кафе
  const [loading, setLoading] = useState(true);           // Загрузка
  const [codes, setCodes] = useState([]);                 // QR-коды

  useEffect(() => {
    loadCafes();
  }, []);



  if (loading) {
    return <CircularProgress />
  }


  return (
    <div>
      <GridContainer>
        <GridItem xs={12} sm={12} md={8}>
          <Card>
            <CardHeader color="dark">
              <h4 className={classes.cardTitleWhite}>Заведение</h4>

            </CardHeader>
            <CardBody>

              <GridContainer>
                <GridItem xs={12} sm={12} md={12}>
                  <InputLabel>Выберите заведение</InputLabel>
                  <CafeSelect
                    labelText="Выберите заведение"
                    id="cafes"
                    owners={cafes}
                    selected={selectCafe}
                    handleChange={(event) => {
                      setSelectCafe(event.target.value);
                      // setMenuItems(event.target.value.menu);

                      loadTables(event.target.value._id);
                    }}
                    formControlProps={{
                      fullWidth: true,
                    }}
                  />
                </GridItem>
              </GridContainer>
            </CardBody>
            <CardFooter>

            </CardFooter>
          </Card>
        </GridItem>
        {
          !codes.length ? null : (
            <GridItem xs={12} sm={12} md={8}>
              <Card>
                <CardHeader color="primary">
                  <h4 className={classes.cardTitleWhite}>Столы</h4>
                </CardHeader>
                <CardBody>

                  <QRTables
                    tableHeaderColor="primary"
                    tableHead={Object.keys(codes[0])}
                    tableData={codes}
                    handleClick={copy}
                  />
                </CardBody>
              </Card>
            </GridItem>
          )
        }

      </GridContainer>
    </div >
  );


  async function loadCafes(cafe_id) {
    try {
      const { data } = await getCafes();
      setCafes(data.items);
      setLoading(false);
    } catch (e) {
      console.log(e);
      setLoading(false);
    }
  }

  -  function copy(text) {
    navigator.clipboard.writeText(text);
  }

  async function loadTables(cafe_id) {
    try {
      const { data } = await _getTableCodes(cafe_id);
      if (!data) {
        return alert('Ошибка сервера');
      }

      if (!data.result) {
        return alert('Нет столов');
      }

      if (data.status === 'ERROR') {
        return alert(data.message);
      }

      if (data.status === 'OK') {
        return setCodes(data.result);
      }
    } catch (e) {
      console.log(e);
    }
  }
}

QRPage.layout = Admin;

export default QRPage;
