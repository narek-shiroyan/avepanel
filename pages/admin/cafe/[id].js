// @material-ui/core components
import { CircularProgress } from "@material-ui/core";
import IconButton from "@material-ui/core/IconButton";
import InputLabel from "@material-ui/core/InputLabel";
import { makeStyles } from "@material-ui/core/styles";
import Close from "@material-ui/icons/Close";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import CardHeader from "components/Card/CardHeader.js";
import Button from "components/CustomButtons/Button.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import CustomSelect from "components/CustomSelect/CustomSelect";
import GridContainer from "components/Grid/GridContainer.js";
// core components
import GridItem from "components/Grid/GridItem.js";
import ImagePicker from "components/ImagePicker/ImagePicker";
// layout for this page
import Admin from "layouts/Admin.js";
import { getAdmins } from 'networks/admin';
import { getCafeById, updateCafe } from 'networks/cafe';
import { useRouter } from 'next/router';
import React, { useEffect, useState } from "react";
import { addCafeImage, deleteCafeImage } from "networks/cafe";
import ObjectTable from 'components/Table/ObjectTable';
import { addTable } from 'networks/cafe';
import { deleteTable } from "networks/cafe";


const styles = {
  cardCategoryWhite: {
    color: "rgba(255,255,255,.62)",
    margin: "0",
    fontSize: "14px",
    marginTop: "0",
    marginBottom: "0",
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
  },
};

function Cafe(props) {
  const useStyles = makeStyles(styles);
  const classes = useStyles();
  const router = useRouter()
  const { id } = router.query;

  const [cafe, setCafe] = useState([]);
  const [admins, setAdmins] = useState([]);
  const [loading, setLoading] = useState(true);

  const [table, setTable] = useState({
    table: 0,
    tableName: '',
  });

  // Переменная для отправки на сервер
  const [info, setInfo] = useState({
    //pickedAdmin: null,
    name: null,
    description: null,
    email: null,
    address: null,
    work_time: null,
    owner: null,
    website: null,
    lat: null,
    lon: null,
    phone: null,
  })
  useEffect(() => {
    loadData();
  }, []);

  if (loading || !cafe) {
    return <CircularProgress />
  }

  // console.log('CAFE', cafe);
  return (
    <div>
      <GridContainer>
        <GridItem xs={12} sm={12} md={6}>
          <h1>{cafe.name}</h1>
          <Card>
            <CardHeader color="dark">
              <h4 className={classes.cardTitleWhite}>Добавить заведение</h4>
              <p className={classes.cardCategoryWhite}>Введите данные</p>
            </CardHeader>
            <CardBody>
              <GridContainer>
                <GridItem xs={12} sm={12} md={12}>
                  <CustomInput
                    labelText="Название"
                    id="title"
                    formControlProps={{
                      fullWidth: true,
                    }}
                    onChange={(event) => setCafe({
                      ...cafe,
                      name: event.target.value,
                    })}
                    inputProps={{
                      // disabled: true,
                      value: cafe.name
                    }}
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={12}>
                  <CustomInput
                    labelText="Описание"
                    id="description"
                    formControlProps={{
                      fullWidth: true,
                    }}
                    onChange={(event) => setCafe({
                      ...cafe,
                      description: event.target.value,
                    })}
                    inputProps={{
                      multiline: true,
                      rows: 5,
                      value: cafe.description
                    }}
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={6}>
                  <CustomInput
                    labelText="Email"
                    id="email"
                    value={cafe.email}
                    onChange={(event) => setCafe({
                      ...cafe,
                      email: event.target.value,
                    })}
                    formControlProps={{
                      fullWidth: true,
                    }}
                    inputProps={{
                      value: cafe.email
                    }}
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={6}>
                  <CustomInput
                    labelText="Режим работы"
                    id="work"
                    onChange={(event) => setCafe({
                      ...cafe,
                      work_time: event.target.value,
                    })}
                    inputProps={{
                      value: cafe.work
                    }}
                    formControlProps={{
                      fullWidth: true,
                    }}
                  />
                </GridItem>
              </GridContainer>
              <GridContainer>
                <GridItem xs={12} sm={12} md={12}>
                  <CustomInput
                    labelText="Адрес"
                    id="address"
                    onChange={(event) => setCafe({
                      ...cafe,
                      address: event.target.value,
                    })}
                    inputProps={{
                      value: cafe.address
                    }}
                    formControlProps={{
                      fullWidth: true,
                    }}
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={6}>
                  <CustomInput
                    labelText="lat"
                    id="lat"
                    onChange={(event) => setCafe({
                      ...cafe,
                      lat: event.target.value,
                    })}
                    inputProps={{
                      value: cafe.lat
                    }}
                    formControlProps={{
                      fullWidth: true,
                    }}
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={6}>
                  <CustomInput
                    labelText="lon"
                    id="lon"
                    inputProps={{
                      value: cafe.lon
                    }}
                    onChange={(event) => setCafe({
                      ...cafe,
                      lon: event.target.value,
                    })}
                    formControlProps={{
                      fullWidth: true,
                    }}
                  />
                </GridItem>
              </GridContainer>
              <GridContainer>
                <GridItem xs={12} sm={12} md={4}>
                  <CustomInput
                    labelText="Сайт"
                    id="site"
                    onChange={(event) => setCafe({
                      ...cafe,
                      website: event.target.value,
                    })}
                    inputProps={{
                      value: cafe.website
                    }}
                    formControlProps={{
                      fullWidth: true,
                    }}
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={4}>
                  <CustomInput
                    labelText="Номер телефона"
                    id="phone"
                    onChange={(event) => setCafe({
                      ...cafe,
                      phone: event.target.value,
                    })}
                    inputProps={{
                      value: cafe.phone
                    }}
                    formControlProps={{
                      fullWidth: true,
                    }}
                  />
                </GridItem>

              </GridContainer>
              <GridContainer>
                <GridItem xs={12} sm={12} md={12}>
                  <InputLabel>Выбрать владельца</InputLabel>
                  <CustomSelect
                    labelText="Владелец"
                    id="owner"
                    owners={admins}
                    selected={cafe.owner}
                    handleChange={(event) => setCafe({
                      ...cafe,
                      owner: event.target.value
                    })}
                    formControlProps={{
                      fullWidth: true,
                    }}
                  />
                </GridItem>

                {/* <GridItem xs={12} sm={12} md={12}>
                  <InputLabel>Выбрать картинки</InputLabel>
                  <ImagePicker
                    id="owner"
                    formControlProps={{
                      fullWidth: true,
                    }}
                  />
                </GridItem> */}
              </GridContainer>
            </CardBody>
            <CardFooter>
              <Button color="dark" onClick={handleUpdateCafe}>Обновить</Button>
            </CardFooter>
          </Card>
        </GridItem>
        <GridItem xs={12} sm={12} md={6}>
          <h1>Картинки</h1>
          <Card>
            <CardHeader color="dark">
              <h4 className={classes.cardTitleWhite}>Добавить картинки</h4>
              <p className={classes.cardCategoryWhite}></p>
            </CardHeader>
            <CardBody>
              <GridContainer>
                <GridItem xs={12} sm={12} md={4}>
                  <InputLabel>Выбрать картинки</InputLabel>
                  <ImagePicker
                    id="owner"
                    item_id={id}
                    handleUploadClick={pickImage}
                    formControlProps={{
                      fullWidth: true,
                    }}
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={12}>
                  {
                    cafe.images.length ? renderImages() : <h3>Нет картинок</h3>
                  }
                </GridItem>
              </GridContainer>



            </CardBody>
            <CardFooter>
              
            </CardFooter>
          </Card>
        </GridItem>

        <GridItem xs={12} sm={12} md={4}>
          <Card>
            <CardHeader color="dark">
              <h4 className={classes.cardTitleWhite}>Столы</h4>
              <p className={classes.cardCategoryWhite}>
                Добавление
              </p>
            </CardHeader>
            <CardBody>
              <GridItem xs={12} sm={12} md={12}>
                <InputLabel>Выбрать владельца</InputLabel>
                <CustomInput
                  labelText="№ стола"
                  id="phone"
                  onChange={(event) => setTable({
                    ...table,
                    table: event.target.value,
                  })}
                  inputProps={{
                    value: table.table,
                    type: 'number'
                  }}
                  formControlProps={{
                    fullWidth: true,
                  }}
                />

                <CustomInput
                  labelText="Место"
                  id="phone"
                  onChange={(event) => setTable({
                    ...table,
                    tableName: event.target.value,
                  })}
                  inputProps={{
                    value: table.tableName,
                  }}
                  formControlProps={{
                    fullWidth: true,
                  }}
                />
                <Button color="dark" onClick={handleAddTable}>Добавить</Button>
              </GridItem>
              {
                !cafe.tables.length ? <h3>Нет столов</h3> : <ObjectTable
                  tableHeaderColor="dark"
                  tableHead={["Стол", "Место"]}
                  tableData={cafe.tables}
                  handleClick={handleRemoveTable}
                />
              }
            </CardBody>

          </Card>
        </GridItem>
      </GridContainer>
    </div>
  );

  async function loadData() {
    setLoading(true);

    try {
      const c = await getCafeById(id);
      const admin = await getAdmins();

      setCafe(c.data.items);
      setAdmins(admin.data.items);
      setLoading(false);

    } catch (e) {
      console.log(e);
      alert('Произошла ошибка');
      setLoading(false);
    }
  }

  async function handleUpdateCafe() {
    setLoading(true);
    try {
      //console.log('data', data);
      await updateCafe(id, cafe);
      setLoading(false);
      await loadData();
    } catch (e) {
      console.log(e);
      setLoading(false);
    }
  }

  async function handleRemoveTable(table_id) {
    try {
      await deleteTable(id, table_id);
      await loadData();
    } catch (error) {
      console.log(error);
      alert('Произошла ошибка');
    }
  }

  async function pickImage(event) {
    var file = event.target.files;


    //  Если выбран 1 файлf
    if (file.length && file.length < 2) {
      const reader = new FileReader();
      var url = reader.readAsDataURL(file[0]);
      reader.onloadend = async function (e) {
        let f = {
          name: file[0].name,
          size: file[0].size,
          type: file[0].type,
          data: reader.result.split(",")[1],
        }
        let formData = new FormData();

        formData.append('name', f.name);
        formData.append('size', f.size);
        formData.append('type', f.type);
        formData.append('data', f.data);
        formData.append('cafe_id', id);


        const { data } = await addCafeImage(formData);
        // console.log('data', data);
        if (data.status === 'OK') {
          alert('Картинки добавлены');
        }
      }.bind(this);
    }

    if (file.length && file.length > 1) {
      for (let i = 0; i < file.length; i++) {
        const reader = new FileReader();
        await reader.readAsDataURL(file[i]);
        reader.onloadend = async function (e) {
          // this.setState({
          //   selectedFile: [reader.result]
          // });
          let f = {
            name: file[i].name,
            size: file[i].size,
            type: file[i].type,
            data: reader.result.split(",")[1],
          }
          let formData = new FormData();
          // console.log('FORM DATA', formData, i);
          formData.append('name', f.name);
          formData.append('size', f.size);
          formData.append('type', f.type);
          formData.append('data', f.data);
          formData.append('cafe_id', id);
          // console.log('FORM DATA', formData);

          const { data } = await addCafeImage(formData);
          if (data.status === 'OK') {
            alert('Картинки добавлены');
          }
          //console.log(await reader.result);
        }.bind(this);
        //return;
      }
    }

    await loadData();
    // console.log('Ошибка');
  }


  function renderTables() {
    return cafe.tables.map((el, key) => (
      <div key={key}>
        <h5>{el.table} - {el.tableName}</h5>
      </div>
    ))
  }

  async function handleAddTable() {
    try {
      await addTable(id, table.table, table.tableName);
      loadData();
    } catch (e) {
      console.log(e);
      alert('Возникла ошибка');
    }
  }

  function renderImages() {
    return cafe.images.map((name, key) => (
      <GridContainer key={key}>
        <GridItem xs={3} sm={3} md={3}>
          <IconButton
            aria-label="Close"
            className={classes.tableActionButton}
            onClick={() => removeImage(name, id)}
          >
            <Close
              className={
                classes.tableActionButtonIcon + " " + classes.close
              }
            />
          </IconButton>
        </GridItem>
        <GridItem xs={9} sm={9} md={9}>
          <img
            src={`https://avemenu.app/api/images/${name}/null/null`}
            key={key}
            style={{
              width: '100%',
              borderRadius: '15px',
              float: 'left',
              alignSelf: 'center',
              marginTop: '20px',
            }}
          />
        </GridItem>

      </GridContainer>))
  }

  async function removeImage(imgName, cafe_id) {
    try {
      await deleteCafeImage(imgName, cafe_id);
      await loadData();
    } catch (e) {
      console.log(e);
    }

  }
}

Cafe.layout = Admin;

export default Cafe;
