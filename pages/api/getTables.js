import connectDB from '../../middleware/mongodb';
import Table from '../../models/table';
import Cafe from '../../models/cafe';

// const handler = nextConnect();
// handler.use(middleware);

const handler = async (req, res) => {
    // if (req.method === 'GET') {
    //     console.log('GET');
    //     // Check if name, email or password is provided
    //     //const { name, email, password } = req.body;
    //     const items = await Cafe.find({});
    //     return res.json({
    //         items
    //     });
    // }
    if (req.method === 'POST') {

        const { cafeId } = req.body;
        //console.log('cafeId', cafeId);
        const items = await Cafe.findOne({ _id: cafeId });
        //console.log('CAFE', items.tables);
        if (!items) {
            return res.json({ status: 'ERROR', message: 'Нет такого заведения' });
        }

        if (!items.tables.length) {
            return res.json({ status: 'ERROR', message: 'Нет столов у заведения' });
        }


        let result = [];
        // Перебрать столы и для каждого узнать id
        for (let i = 0; i < items.tables.length; i++) {
            const tabler = items.tables[i];
            // const { _id } = await Table.findOne({ name: tabler.name});
            // console.log('_id', _id);
            result.push({ name: tabler.tableName, table: tabler.table, url: `https://avemenu.app/code/${tabler._id}` });
            // console.log('table', tabler);
        }

        res.status(200).json({ status: 'OK', result });
    }



    else {
        return res.status(422).send('req_method_not_supported');
    }
};


export default connectDB(handler);