import connectDB from '../../middleware/mongodb';
import Cafe from '../../models/cafe';
import nextConnect from 'next-connect';
import middleware from '../../middleware/middleware';
import fs from 'fs';

const handler = nextConnect();
handler.use(middleware);

handler.post(async (req, res) => {
    if (req.method === 'GET') {
        console.log('GET');
        // Check if name, email or password is provided
        //const { name, email, password } = req.body;
        const items = await Cafe.find({});
        return res.json({
            items
        });
    }
    if (req.method === 'POST') {
        console.log('Create Menu');
        //console.log('body', req.body);
        console.log('files', req.files);
        let cafe_id = req.body.cafe_id[0];
        let menu = {
            name: req.body.name[0],
            group: req.body.group[0],
            description: req.body.description[0],
            portion: req.body.portion[0],
            price: req.body.price[0],
            images: [],
        }


        for (let i = 0; i < req.files.image.length; i++) {
            if (req.files.image[i].size) {
                const element = req.files.image[i];
                // await element.path.mv(`/Users/nareksiroan/Desktop/pics/test${i}.png`);

                let imageName = `${Date.now()}-${i}.png`;
                await fs.readFile(element.path, async function (err, data) {
                    await fs.writeFile(`/Users/narek/Desktop/menu/${imageName}`, data, async function (err) {
                        if (err) {
                            console.log(err);
                        }
                        //await Cafe.updateOne({ _id: cafe_id }, { $push: { images: imageName } });
                    });
                });
                await menu.images.push(imageName);
            }
        }

        await Cafe.updateOne({ _id: cafe_id }, { $push: { menu } });


        console.log('MENU', menu);


        //const { items: { name, description, portion, price } } = req.body;


        res.status(200);
    }



    else {
        return res.status(422).send('req_method_not_supported');
    }
});

export const config = {
    api: {
        bodyParser: false
    }
}

export default connectDB(handler);


/**
 *   cafe_id: [ '61199e261dba084479aa2d16' ],
  name: [ 'фывфвфвфы' ],
  description: [ 'ывффвфвыф' ],
  portion: [ '55' ],
  price: [ '123' ]
 */