import connectDB from '../../middleware/mongodb';
import Admin from '../../models/admin';
const bcrypt = require('bcrypt');

const handler = async (req, res) => {
    if (req.method === 'POST') {

        const { login, password, email } = req.body;
        if (!login || !password || !email) {
            return res.status(500).json({ status: 'ERROR', message: 'Не все поля заполнены' });
        }
        try {
            // хешируем пароль
            const hashPassword = await bcrypt.hash(password, 10);

            const newAdmin = new Admin({
                login,
                password: hashPassword,
                email,
                lastUpdate: Date.now(),
            });

            newAdmin.save();
            return res.status(200).json({ status: 'OK', message: 'Пользователь успешно создан' });
        } catch (e) {
            console.log('e', e);
            return res.status(500).json({ status: 'ERROR', message: e });
        }
    }
    else {
        return res.status(422).send('req_method_not_supported');
    }
};

export default connectDB(handler);