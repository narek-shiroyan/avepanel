import connectDB from '../../middleware/mongodb';
import Admin from '../../models/admin';

const handler = async (req, res) => {
  console.log(req.body);
  if (req.method === 'GET') {
    // Check if name, email or password is provided
    //const { name, email, password } = req.body;
    const items = await Admin.find({});
    // console.log('items', items);
    return res.json({
      items
    });
  }
  else {
    return res.status(422).send('req_method_not_supported');
  }
};

export default connectDB(handler);