import connectDB from '../../middleware/mongodb';
import Cafe from '../../models/cafe';
// const handler = nextConnect();
// handler.use(middleware);

const handler = async (req, res) => {
  if (req.method === 'GET') {
    console.log('GET');
    // Check if name, email or password is provided
    //const { name, email, password } = req.body;
    const items = await Cafe.find({});
    return res.json({
      items
    });
  }
  if (req.method === 'POST') {

    const { items: { name, description, email, address, work_time, owner, tables, website, phone, lat, lon } } = req.body;

    let newTables = [];
    for (let i = 0; i < tables; i++) {
      newTables.push({
        table: i + 1
      });
    }
    const newCafe = new Cafe({
      name,
      description,
      email,
      address,
      work_time,
      owner,
      tables: newTables,
      website,
      phone,
      lat,
      lon,
      status: '',
    });

    await newCafe.save();
    console.log('Завершилось');
    return res.status(200).json({ status: 'OK' });
  }



  else {
    return res.status(422).send('req_method_not_supported');
  }
};


export default connectDB(handler);