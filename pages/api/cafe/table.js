import connectDB from 'middleware/mongodb';
import Cafe from 'models/cafe';
// const handler = nextConnect();
// handler.use(middleware);

const handler = async (req, res) => {

  if (req.method === 'POST') {
    const { table, tableName, id } = req.body;
    try {
      await Cafe.updateOne({ _id: id }, { $push: { tables: { table, tableName } } });
      return res.status(200).json({ status: 'OK' });
    } catch (e) {
      console.log(e);
      return res.status(500).json({ status: 'ERROR' });
    }
  }


  if (req.method === 'DELETE') {
    const { cafe_id, table_id } = req.body;
    try {
      await Cafe.updateOne({ _id: cafe_id }, { $pull: { tables: { _id: table_id } } });
      return res.status(200).json({ status: 'OK' });
    } catch (e) {
      console.log(e);
      return res.status(500).json({ status: 'ERROR' });
    }
  }

}

export default connectDB(handler);