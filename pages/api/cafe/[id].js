import connectDB from 'middleware/mongodb';
import Cafe from 'models/cafe';
// const handler = nextConnect();
// handler.use(middleware);

const handler = async (req, res) => {
  if (req.method === 'GET') {
    const { id } = req.query;
    
    try {
      const items = await Cafe.findOne({ _id: id });
      if (items) {
        return res.json({
          status: 'OK',
          items
        });
      } else {
        console.log('id не найден');
        return res.status(500).json({ status: 'ERROR' });
      }
    } catch (e) {
      return res.status(500).json({ status: 'ERROR' });
    }
  }


  if (req.method === 'PUT') {
    const { info, id } = req.body;
    try {
      await Cafe.updateOne({ _id: id }, { $set: info });
      return res.status(200).json({ status: 'OK' });
    } catch (e) {
      console.log(e);
      return res.status(500).json({ status: 'ERROR ' });
    }
  }

  else {
    return res.status(422).send('req_method_not_supported');
  }
};


export default connectDB(handler);