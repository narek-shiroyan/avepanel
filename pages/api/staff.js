import connectDB from '../../middleware/mongodb';
import Staff from '../../models/staff';
import { v4 as uuidv4 } from 'uuid';


const bcrypt = require('bcrypt');

const handler = async (req, res) => {
    if (req.method === 'POST') {

        const { login, password, cafe } = req.body;
        if (!login || !password || !cafe) {
            return res.status(500).json({ status: 'ERROR', message: 'Не все поля заполнены' });
        }
        try {
            // хешируем пароль
            const hashPassword = await bcrypt.hash(password, 10);

            const newStaff = new Staff({
                login,
                password: hashPassword,
                cafe,
                token: uuidv4(),
            });

            newStaff.save();
            return res.status(200).json({ status: 'OK', message: 'Пользователь успешно создан' });
        } catch (e) {
            console.log('e', e);
            return res.status(500).json({ status: 'ERROR', message: e });
        }
    }
    if (req.method === 'GET') {
        // Check if name, email or password is provided
        //const { name, email, password } = req.body;
        const items = await Staff.find({});
        console.log('items', items);
        return res.json({
            items
        });
    }
    else {
        return res.status(422).send('req_method_not_supported');
    }
};

export default connectDB(handler);