import connectDB from '../../middleware/mongodb';
import Table from '../../models/table';
import Cafe from '../../models/cafe';

// const handler = nextConnect();
// handler.use(middleware);

const handler = async (req, res) => {
    // if (req.method === 'GET') {
    //     console.log('GET');
    //     // Check if name, email or password is provided
    //     //const { name, email, password } = req.body;
    //     const items = await Cafe.find({});
    //     return res.json({
    //         items
    //     });
    // }
    if (req.method === 'POST') {

        const { name, table, cafeId } = req.body;
        if (!name || !table || !cafeId) {
            return res.status(500);
        }

        // const newTable = new Table({
        //     name,
        // });
        await Cafe.updateOne({ _id: cafeId }, { $push: { tables: { table, tableName: name } } })
        // newTable.save();

        res.status(200);
    }



    else {
        return res.status(422).send('req_method_not_supported');
    }
};


export default connectDB(handler);