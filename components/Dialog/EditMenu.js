import React, { useState } from 'react';
import PropTypes from "prop-types";
import {
    DialogContent,
    Dialog,
    DialogActions,
    DialogContentText,
    DialogTitle
} from '@material-ui/core';
import { makeStyles } from "@material-ui/core/styles";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardAvatar from "components/Card/CardAvatar.js";
import CardFooter from "components/Card/CardFooter.js";
import CardHeader from "components/Card/CardHeader.js";
import Button from "components/CustomButtons/Button.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import InputLabel from "@material-ui/core/InputLabel";
import GridContainer from "components/Grid/GridContainer.js";
import ImagePicker from "components/ImagePicker/ImagePicker";
import { CircularProgress } from "@material-ui/core";
import CustomSelect from "../../components/CustomSelect/CustomSelect";
// core components
import GridItem from "components/Grid/GridItem.js";
import { updateMenu } from 'networks/menu';



const styles = {
    cardCategoryWhite: {
        color: "rgba(255,255,255,.62)",
        margin: "0",
        fontSize: "14px",
        marginTop: "0",
        marginBottom: "0",
    },
    cardTitleWhite: {
        color: "#FFFFFF",
        marginTop: "0px",
        minHeight: "auto",
        fontWeight: "300",
        fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
        marginBottom: "3px",
        textDecoration: "none",
    },
};



const EditMenuDialog = (props) => {
    const useStyles = makeStyles(styles);
    const classes = useStyles();
    const { show, handleClose, cafe_id, handleRefresh, menu } = props;
    const [food, setFood] = useState(null);
    const [file, setFile] = useState(null);
    console.log('file', file);
    if (!show) {
        return null;
    }

    if (!menu) {
        return null;
    }
    return (
        <div>
            <Dialog
                open={show}
                onClose={handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                
                <DialogTitle id="alert-dialog-title">
                    {"Форма создания"}
                </DialogTitle>
                <DialogContent>
                    <Card>
                        <CardHeader color="primary">
                            <h4 className={classes.cardTitleWhite}>Добавить блюдо</h4>
                            <p className={classes.cardCategoryWhite}>Введите данные</p>
                        </CardHeader>
                        <CardBody>
                            <GridContainer>
                                <GridItem xs={12} sm={12} md={4} style={{ marginTop: 50 }}>
                                <CardAvatar profile >
                                    <a href="#pablo" onClick={(e) => e.preventDefault()}>
                                        {
                                                menu.images.length ? (
                                                <>
                                                    <img
                                                            src={`https://avemenu.app/api/images/${menu.images[0]}/500/500`}
                                                            alt={`${menu.name}`} />
                                                </>
                                            ) : (
                                                <p>Нет картинки</p>
                                            )
                                        }
                                    </a>
                                </CardAvatar>
                                </GridItem>
                                <GridItem xs={12} sm={12} md={4} style={{ marginTop: 50 }}>
                                    <InputLabel>Выбрать картинки</InputLabel>
                                    <ImagePicker
                                        id="owner"
                                        handleUploadClick={pickImage}
                                        formControlProps={{
                                            fullWidth: true,
                                        }}
                                        solo={true}
                                    />
                                </GridItem>
                                <GridItem xs={12} sm={12} md={12}>
                                    <CustomInput
                                        labelText="Название"
                                        id="title"
                                        formControlProps={{
                                            fullWidth: true,
                                        }}
                                        onChange={(event) => setFood({
                                            ...food,
                                            name: event.target.value,
                                        })}
                                        inputProps={{
                                            // disabled: true,
                                            value: food ? food.name : menu.name,
                                        }}
                                    />
                                </GridItem>
                                <GridItem xs={12} sm={12} md={12}>
                                    <CustomInput
                                        labelText="Описание"
                                        id="description"
                                        formControlProps={{
                                            fullWidth: true,
                                        }}
                                        onChange={(event) => setFood({
                                            ...food,
                                            description: event.target.value,
                                        })}
                                        inputProps={{
                                            multiline: true,
                                            rows: 5,
                                            value: food ? food.description : menu.description,
                                        }}
                                    />
                                </GridItem>
                                <GridItem xs={12} sm={12} md={6}>
                                    <CustomInput
                                        labelText="Стоимость"
                                        id="price"
                                        formControlProps={{
                                            fullWidth: true,
                                        }}
                                        onChange={(event) => setFood({
                                            ...food,
                                            price: event.target.value,
                                        })}
                                        inputProps={{
                                            // disabled: true,
                                            type: 'number',
                                            value: food ? food.price : menu.price,
                                        }}
                                    />
                                </GridItem>
                                <GridItem xs={12} sm={12} md={6}>
                                    <CustomInput
                                        labelText="Категория"
                                        id="title"
                                        formControlProps={{
                                            fullWidth: true,
                                        }}
                                        onChange={(event) => setFood({
                                            ...food,
                                            group: event.target.value,
                                        })}
                                        inputProps={{
                                            // disabled: true,
                                            value: food ? food.group : menu.name,
                                        }}
                                    />
                                </GridItem>
                            </GridContainer>



                        </CardBody>
                    </Card>
                </DialogContent>
                <DialogActions>
                    <Button onClick={sendToCreate}>Обновить</Button>
                    <Button onClick={handleClose} autoFocus>
                        Отмена
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    )



    async function pickImage(event) {
        var file = event.target.files;

        if (!file.length) {
            return;
        }

        //  Если выбран 1 файлf
        const reader = new FileReader();
        var url = reader.readAsDataURL(file[0]);

        reader.onloadend = async function (e) {
            setFile({
                name: file[0].name,
                size: file[0].size,
                type: file[0].type,
                data: reader.result.split(",")[1],
            });


            //   const { data } = await addCafeImage(formData);
            //   // console.log('data', data);
            //   if (data.status === 'OK') {
            //       alert('Картинки добавлены');
            //   }
        }.bind(this);

        // console.log('Ошибка');
    }


    async function sendToCreate() {

        let formData = new FormData();
        formData.append('cafe_id', cafe_id);

        formData.append('food_id', menu._id);
        formData.append('items', JSON.stringify(food));

        if (file) {
            formData.append('file', JSON.stringify(file));
        }

        try {
            const { data } = await updateMenu(formData);
            console.log('data', data);
            if (data.status === 'ERROR') {
                alert(data.message);
            }
            if (data.status === 'OK') {
                // Нужно обновить
                await handleRefresh();

            }
        } catch (e) {
            console.log(e);
        }
    }

};


EditMenuDialog.propTypes = {
    show: PropTypes.bool.isRequired,
    cafe_id: PropTypes.string,
    handleClose: PropTypes.func,
    handleRefresh: PropTypes.func,
    menu: PropTypes.object,
}


export default EditMenuDialog;