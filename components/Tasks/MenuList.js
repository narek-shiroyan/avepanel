import IconButton from "@material-ui/core/IconButton";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import Tooltip from "@material-ui/core/Tooltip";
import Close from "@material-ui/icons/Close";
// @material-ui/icons
import Edit from "@material-ui/icons/Edit";
// core components
import styles from "assets/jss/nextjs-material-dashboard/components/tasksStyle.js";
import classnames from "classnames";
import CardAvatar from "components/Card/CardAvatar.js";
import PropTypes from "prop-types";
import React from "react";



const customStyles = {
    image: {
        width: 100,
        height: 100,
    }
};


export default function MenuList(props) {
    const useStyles = makeStyles([styles, customStyles]);
    const classes = useStyles();
    const { list, rtlActive, click, deleteFood, cafe_id } = props;
    const tableCellClasses = classnames(classes.tableCell, {
        [classes.tableCellRTL]: rtlActive,
    });
    return (
        <Table className={classes.table}>
            <TableBody>
                {list.map((value, key) => (
                    <TableRow key={key} className={classes.tableRow}>
                        <TableCell className={tableCellClasses}>
                            <CardAvatar profile style={{ width: 50, height: 50, margin: 0 }}>

                                {
                                    value.images.length ? (
                                        <>
                                            <img
                                                src={`https://avemenu.app/api/images/${value.images[0]}/100/100`}
                                                alt={`${value.name}`}
                                            />
                                        </>
                                    ) : null
                                }

                            </CardAvatar>
                        </TableCell>
                        <TableCell className={tableCellClasses}>{value.name}</TableCell>
                        <TableCell className={classes.tableActions}>
                            <Tooltip
                                id="tooltip-top"
                                title="Edit Task"
                                placement="top"
                                classes={{ tooltip: classes.tooltip }}
                            >
                                <IconButton
                                    aria-label="Edit"
                                    className={classes.tableActionButton}
                                    onClick={() => click(value)}
                                // href={`/admin/cafe/${value._id}`}
                                >
                                    <Edit
                                        className={
                                            classes.tableActionButtonIcon + " " + classes.edit
                                        }

                                    />
                                </IconButton>
                            </Tooltip>
                            <Tooltip
                                id="tooltip-top-start"
                                title="Remove"
                                placement="top"
                                classes={{ tooltip: classes.tooltip }}
                            >
                                <IconButton
                                    aria-label="Close"
                                    className={classes.tableActionButton}
                                    onClick={() => deleteFood(cafe_id, value._id, value.images.length ? value.images[0] : null)}
                                >
                                    <Close
                                        className={
                                            classes.tableActionButtonIcon + " " + classes.close
                                        }
                                    />
                                </IconButton>
                            </Tooltip>
                        </TableCell>
                    </TableRow>
                ))}
            </TableBody>
        </Table>
    );
}

MenuList.propTypes = {
    tasksIndexes: PropTypes.arrayOf(PropTypes.number),
    list: PropTypes.arrayOf(PropTypes.object),
    rtlActive: PropTypes.bool,
    checkedIndexes: PropTypes.array,
    click: PropTypes.func,
};
