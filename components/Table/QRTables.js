import React from "react";
import PropTypes from "prop-types";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import IconButton from "@material-ui/core/IconButton";
import CopyIcon from "@material-ui/icons/FileCopyOutlined";

// core components
import styles from "assets/jss/nextjs-material-dashboard/components/tableStyle.js";

export default function QRTables(props) {
    const useStyles = makeStyles(styles);
    const classes = useStyles();
    const { tableHead, tableData, tableHeaderColor, handleClick } = props;

    return (
        <div className={classes.tableResponsive}>
            <Table className={classes.table}>
                {tableHead !== undefined ? (
                    <TableHead className={classes[tableHeaderColor + "TableHeader"]}>
                        <TableRow className={classes.tableHeadRow}>
                            {tableHead.map((prop, key) => {
                                return (
                                    <TableCell
                                        className={classes.tableCell + " " + classes.tableHeadCell}
                                        key={key}
                                    >
                                        {prop}
                                    </TableCell>
                                );
                            })}
                        </TableRow>
                    </TableHead>
                ) : null}
                <TableBody>
                    {tableData.map((prop, key) => {
                        return (
                            <TableRow key={key} className={classes.tableBodyRow}>
                                <>
                                {
                                    Object.keys(prop).map((el, key) => (
                                        <TableCell key={key} className={classes.tableCell}>
                                            {prop[el]}
                                        </TableCell>
                                    ))
                                }

                                    <TableCell className={classes.tableCell}>
                                        <IconButton
                                            aria-label="Copy"
                                            className={classes.tableActionButton}
                                            onClick={() => handleClick(prop.url)}
                                        >
                                            <CopyIcon
                                                className={
                                                    classes.tableActionButtonIcon + " " + classes.close
                                                }
                                            />
                                        </IconButton>
                                    </TableCell>

                                </>
                            </TableRow>
                        );
                    })}
                </TableBody>
            </Table>
        </div>
    );

}

QRTables.defaultProps = {
    tableHeaderColor: "gray",
};

QRTables.propTypes = {
    tableHeaderColor: PropTypes.oneOf([
        "warning",
        "primary",
        "danger",
        "success",
        "info",
        "rose",
        "gray",
    ]),
    tableHead: PropTypes.arrayOf(PropTypes.string),
    tableData: PropTypes.arrayOf(PropTypes.object),
};
