const withPlugins = require("next-compose-plugins");
const withImages = require("next-images");
const webpack = require("webpack");
const path = require("path");

module.exports = withPlugins([[withImages]], {
    webpack(config, options) {
        config.resolve.modules.push(path.resolve("./"));
        return config;
    },
    env: {
        mongodburl: "mongodb://127.0.0.1/avemenu",
    },
    images: {
        disableStaticImages: true
    },
    future: {
        webpack: false,
    }
});
