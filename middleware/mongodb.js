import mongoose from 'mongoose';
const tunnel = require('tunnel-ssh');

import fs from 'fs';

const config = {
  username: 'root',
  host: '94.228.116.169',
  privateKey: fs.readFileSync('/Users/narek/.ssh/id_rsa'),
  port: 22,
  dstPort: 27017,
  password: 'pq2GKARF',
  passphrase: 'narek'
};

const connectDB = handler => async (req, res) => {
  if (mongoose.connections[0].readyState) {
    // Use current db connection
    console.log('MONGO READY STATE');
    return handler(req, res);
  }
  // Use new db connection

  await tunnel(config, async function (error, server) {
    if (error) {
      console.log("SSH connection error: " + error);
    }
    await mongoose.connect('mongodb://127.0.0.1/avemenu', {
      useUnifiedTopology: true,
      //useFindAndModify: false,
      //useCreateIndex: true,
      useNewUrlParser: true
    });
  });
  console.log('ВРОДЕ ВСЕ ПРОШЛО');
  return handler(req, res);
};

export default connectDB;