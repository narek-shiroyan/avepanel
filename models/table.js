import mongoose from 'mongoose';

const Table = new mongoose.Schema({
    id: Number,
    name: String,
});

mongoose.models = {};

export default mongoose.model("Table", Table);