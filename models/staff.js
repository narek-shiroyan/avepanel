import mongoose from 'mongoose';

const Staff = new mongoose.Schema({
    id: Number,
    login: String,
    token: String,
    password: String,
    date: {
        type: String,
        default: Date.now(),
    },
    push: Array,
    cafe: String,
});
mongoose.models = {};

export default mongoose.model("Staff", Staff);
