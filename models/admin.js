import mongoose from 'mongoose';

const Admin = new mongoose.Schema({
    id: Number,
    login: String,
    lastUpdate: String,
    token: String,
    email: String,
    password: String,
    role: String,
    date: {
        type: String,
        default: Date.now(),
    },
    push: Array,
});
mongoose.models = {};

export default mongoose.model("Admin", Admin);
