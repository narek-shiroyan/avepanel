import mongoose from 'mongoose';


const SchemaTypes = mongoose.Schema.Types;


const Cafe = new mongoose.Schema({
    id: Number,
    name: String,
    description: String,
    owner: String,
    email: String,
    address: String,
    lat: String,
    lon: String,
    work_time: String,
    payStatus: String,
    status: String,
    website: String,
    phone: String,
    menu: {
        type: [new mongoose.Schema({
            id: Number,
            name: String,
            description: String,
            portion: String,
            variants: {
                type: [],
                default: [],
            },
            images: [],
            price: Number,
            group: String,
        })],
        default: [],
    },
    date: {
        type: String,
        default: Date.now(),
    },
    tables: {
        type: [new mongoose.Schema({
            table: Number,
            tableName: String,
        })],
        default: [],
    }, // Сюда кидаем название помещения и номер стола
    images: Array,
});

mongoose.models = {};


/*
    Menu
    - name - string         - Название блюда
    - description - string  - Описание блюда
    - portion - string      - Порция
    - variant? - []         - Если вариативный товар, добавить варианты
        - title: {
                name: string,
                price: number,
        }

    - images - []           - Картинки
    - price number          - Стоимость товара

*/

export default mongoose.model("Cafes", Cafe);
